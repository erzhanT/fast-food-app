import React, {useState} from "react";
import "./App.css";
import FoodMenu from "../../components/FoodMenu/FoodMenu";
import Order from '../../components/Order/Order';

const App = () => {

  const [menu, setMenu] = useState([
    { id: 1, name: "Hamburger", price: 80, count: 0 },
    { id: 2, name: "Cheeseburger", price: 90, count: 0 },
    { id: 3, name: "Fries", price: 45, count: 0 },
    { id: 4, name: "Coffee", price: 70, count: 0 },
    { id: 5, name: "Tea", price: 50, count: 0 },
    { id: 6, name: "Milk", price: 50, count: 0 }
  ]);


  return (
      <div className="App">
        <Order menu={menu} setMenu={(v) => setMenu(v)} />
        <FoodMenu menu={menu} setMenu={(v) => setMenu(v)} />
      </div>
  );

}






export default App;
