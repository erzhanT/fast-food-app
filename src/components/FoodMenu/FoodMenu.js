import React from 'react';

const FoodMenu = props => {
    const { menu, setMenu } = props;
    const handleClick = index => {
        const  menuCopy = [...menu];
        menuCopy[index].count += 1;
        setMenu(menuCopy);
    };

    return (
        <div>
            {menu.map((el, ind) => {
                const { id, name, price, count } = el;
                return (
                    <div className="product" key={id} onClick={() => handleClick(ind)}>
                        <div className="name">{name}</div>
                        <div className="price">Price: {price} KGS</div>
                        <div className="price">Count: {count}</div>
                    </div>
                );
            })}
        </div>
    );
};

export default FoodMenu;