import React from 'react';


const Order = props => {
    const {menu, setMenu} = props;
    const result = menu.filter((item) => item.count > 0);
    const removeOrder = id => {
        const index = menu.findIndex(i => i.id === id);
        const menuCopy = [...menu];
        menuCopy[index].count = 0;
        setMenu(menuCopy);

    };

    const totalSpent = () => {
        return menu.reduce((acc, menu) => {
            if (menu.count > 0) {
                return acc + (menu.price * menu.count);
            }
            return acc;
        }, 0)
    };

    return (
        <div>
            {result.map((item) => {
                const {id, name, price, count} = item;

                return (
                    <div className="box">
                        <div className="order">
                            {name} - {price} KGS. count ({count}) = {count * price}{" "} KGS
                            <button onClick={() => removeOrder(id)}>Delete</button>
                        </div>

                    </div>
                );
            })}
            <div><strong>Total : {totalSpent()} KGS</strong></div>

        </div>
    );
};

export default Order;